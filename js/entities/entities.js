// Items that persist through the levels. Everything gets reset on a level by level basis
game.persistent = {
		element_active: false,
};
//game.persistent.HUD_image

game.ElemEnum = {
    FIRE : 1,
    WATER : 2,
    WIND : 3,
    METAL : 4,
    ELECTRIC : 5
}

/*------------------- 
a player entity
-------------------------------- */
game.PlayerEntity = me.ObjectEntity.extend({
 
    /* -----
 
    constructor
 
    ------ */
 
    init: function(x, y, settings) {
        // call the constructor
        this.parent(x, y, settings);

		// adjust the bounding box
		this.updateColRect(8, 48, -1, 0);
		
		// define the animations
		this.renderable.addAnimation("stone", [0,1,2,3,4,5,6,7]);
		this.renderable.addAnimation("idle_stone", [0]);
		this.renderable.addAnimation("fire", [8,9,10,11]);
		this.renderable.addAnimation("water", [16,17,18,19,20]);
		this.renderable.addAnimation("wind", [24,25,26,27]);
		this.renderable.addAnimation("metal", [32,33,34,35]);
		this.renderable.addAnimation("elec", [40,41,42,43]);
		this.renderable.animationspeed=me.sys.fps/1;
		this.fullReset();
		
        // set the display to follow our position on both axis
        me.game.viewport.follow(this.pos, me.game.viewport.AXIS.BOTH);
    },

	/*reset everything to stone values*/
	fullReset: function() {
			// set the default horizontal & vertical speed (accel vector)
			this.setVelocity(3, 15); 
			this.renderable.setCurrentAnimation("idle_stone");
			game.persistent.element_active=false;
			this.updateColRect(8, 48, -1, 0);
			this.canBreakTile=false;
	},
	
/* -----
update the player pos
------ */
update: function() {
	if (me.input.isKeyPressed('toggleElement')&&(me.game.HUD.getItemValue("element")!=0))	
    {
        me.audio.play("burp2");
		game.persistent.element_active=!game.persistent.element_active;
		if (game.persistent.element_active){
			switch(me.game.HUD.getItemValue("element")){
				case 1: //fire
				this.renderable.setCurrentAnimation("fire");
				this.updateColRect(8, 48, -1, 0);
				this.canBreakTile=true;
				break;
				case 2: //water
				this.renderable.setCurrentAnimation("water");
				this.updateColRect(8, 48, -1, 0);
				this.canBreakTile=false;
				;break;
				case 3: //wind
				this.renderable.setCurrentAnimation("wind");
				this.updateColRect(8, 48, -1, 0);
				this.setVelocity(6, 20); 
				this.canBreakTile=false;
				break;
				case 4: //metal
				this.renderable.setCurrentAnimation("metal");
				this.updateColRect(8, 48, -1, 0);
				this.setVelocity(2, 10);
				this.canBreakTile=false;
				break;
				case 5: //elec
				this.renderable.setCurrentAnimation("elec");
				this.updateColRect(8, 48, -1, 0);
				this.canBreakTile=false;
				break;
				default: break;
			}
		}
		else {
			// reset everything to stone values
			this.fullReset();
		}
    }
	
    if (me.input.isKeyPressed('left'))
    {
        // flip the sprite on horizontal axis
        this.flipX(true);
        // update the entity velocity
        this.vel.x -= this.accel.x * me.timer.tick;
    }
    else if (me.input.isKeyPressed('right'))
    {
        // unflip the sprite
        this.flipX(false);
        // update the entity velocity
        this.vel.x += this.accel.x * me.timer.tick;
    }
    else
    {
        this.vel.x = 0;
    }
    if (me.input.isKeyPressed('jump'))
    {   
        if (!this.jumping && !this.falling) 
        {
            // set current vel to the maximum defined value
            // gravity will then do the rest
            this.vel.y = -this.maxVel.y * me.timer.tick;
            // set the jumping flag
            this.jumping = true;
            me.audio.play("jump");
        }
    }
 
 
    // check & update player movement
    this.updateMovement();
 
    // check for collision
    var res = me.game.collide(this);
 
    if (res) {
        // if we collide with an enemy
        if (res.obj.type == me.game.ENEMY_OBJECT) {
            // check if we jumped on it
            if ((res.y > 0) && ! this.jumping) {
                // bounce (force jump)
                this.falling = false;
                this.vel.y = -this.maxVel.y * me.timer.tick;
                // set the jumping flag
                this.jumping = true;
 
            } else {
                // let's flicker in case we touched an enemy
                this.renderable.flicker(45);
            }
        }
    }
	
	if (game.persistent.element_active==false){
		if ((this.vel.x!=0 || this.vel.y!=0)) {
			if(this.renderable!=null){
			this.renderable.setCurrentAnimation("stone");
			}
		}
		else if((this.vel.x==0)&&(this.vel.y==0)) {
			this.renderable.setCurrentAnimation("idle_stone");
	}
	}
    // update animation if necessary
    //if (this.vel.x!=0 || this.vel.y!=0) {
        // update object animation
        this.parent();
        return true;
    //}
    // else inform the engine we did not perform
    // any update (e.g. position, animation)
    //return false;       
 
}
 
});

/*----------------
 a Coin entity
------------------------ */
game.CoinEntity = me.CollectableEntity.extend({
    // extending the init function is not mandatory
    // unless you need to add some extra initialization
    init: function(x, y, settings) {
        // call the parent constructor
        this.parent(x, y, settings);
    },
 
    // this function is called by the engine, when
    // an object is touched by something (here collected)
	onCollision : function ()
	{
		// do something when collected
	 
		// give some score
		me.game.HUD.updateItemValue("score", 250);

		// make sure it cannot be collected "again"
		this.collidable = false;
		// remove it
		me.game.remove(this);
	}
 
});

/* --------------------------
an enemy Entity
------------------------ */
game.EnemyEntity = me.ObjectEntity.extend({
    init: function(x, y, settings) {
        // define this here instead of tiled
        settings.image = "wheelie_right";
        settings.spritewidth = 64;
 
        // call the parent constructor
        this.parent(x, y, settings);
 
        this.startX = x;
        this.endX = x + settings.width - settings.spritewidth;
        // size of sprite
 
        // make him start from the right
        this.pos.x = x + settings.width - settings.spritewidth;
        this.walkLeft = true;
 
        // walking & jumping speed
        this.setVelocity(4, 6);
 
        // make it collidable
        this.collidable = true;
        // make it a enemy object
        this.type = me.game.ENEMY_OBJECT;
 
    },
 
    // call by the engine when colliding with another object
    // obj parameter corresponds to the other object (typically the player) touching this one
    onCollision: function(res, obj) {
 
        // res.y >0 means touched by something on the bottom
        // which mean at top position for this one
        if (this.alive && (res.y > 0) && obj.falling) {
            this.renderable.flicker(45);
        }
    },
 
    // manage the enemy movement
    update: function() {
        // do nothing if not in viewport
        if (!this.inViewport)
            return false;
 
        if (this.alive) {
            if (this.walkLeft && this.pos.x <= this.startX) {
                this.walkLeft = false;
            } else if (!this.walkLeft && this.pos.x >= this.endX) {
                this.walkLeft = true;
            }
            // make it walk
            this.flipX(this.walkLeft);
            this.vel.x += (this.walkLeft) ? -this.accel.x * me.timer.tick : this.accel.x * me.timer.tick;
                 
        } else {
            this.vel.x = 0;
        }
         
        // check and update movement
        this.updateMovement();
         
        // update animation if necessary
        if (this.vel.x!=0 || this.vel.y!=0) {
            // update object animation
            this.parent();
            return true;
        }
        return false;
    }
});

/*-------------- 
an element slot HUD Item
--------------------- */
 
game.ElementSlot = me.HUD_Item.extend({
    init: function(x, y) {
        // call the parent constructor
        this.parent(x, y);
		this.image=me.loader.getImage('Eslot_empty');
		this.value=0;
    },
 
    draw: function(context, x, y) {
		switch(this.value){
		case 0: this.image=me.loader.getImage('Eslot_empty'); break;
		case 1: this.image=me.loader.getImage('Eslot_fire'); break;
		case 2: this.image=me.loader.getImage('Eslot_water'); break;
		case 3: this.image=me.loader.getImage('Eslot_wind'); break;
		case 4: this.image=me.loader.getImage('Eslot_metal'); break;
		case 5: this.image=me.loader.getImage('Eslot_elec'); break;
		default: break;
		}
		context.drawImage(this.image,this.pos.x,this.pos.y);
    }
 
});

/*----------------
 Several classes for totems
------------------------ */

/*----------------
Fire totem 1
------------------------ */
game.FireTotem = me.CollectableEntity.extend({
    // extending the init function is not mandatory
    // unless you need to add some extra initialization
    init: function(x, y, settings) {
        // call the parent constructor
        this.parent(x, y, settings);
    },
 
    // this function is called by the engine, when
    // an object is touched by something (here collected)
	onCollision : function ()
	{
		if(me.game.HUD.getItemValue("element")!=1){
			me.game.HUD.setItemValue("element", 1);
			// reset everything to stone values
			me.game.getEntityByName("mainPlayer")[0].fullReset();
		}
	}
 
});

/*----------------
Water totem 2
------------------------ */
game.WaterTotem = me.CollectableEntity.extend({
    // extending the init function is not mandatory
    // unless you need to add some extra initialization
    init: function(x, y, settings) {
        // call the parent constructor
        this.parent(x, y, settings);
    },
 
    // this function is called by the engine, when
    // an object is touched by something (here collected)
	onCollision : function ()
	{
		if(me.game.HUD.getItemValue("element")!=2){
			me.game.HUD.setItemValue("element", 2);
			// reset everything to stone values
			me.game.getEntityByName("mainPlayer")[0].fullReset();
		}
	}
 
});

/*----------------
Wind totem 3
------------------------ */
game.WindTotem = me.CollectableEntity.extend({
    // extending the init function is not mandatory
    // unless you need to add some extra initialization
    init: function(x, y, settings) {
        // call the parent constructor
        this.parent(x, y, settings);
    },
 
    // this function is called by the engine, when
    // an object is touched by something (here collected)
	onCollision : function ()
	{
		if(me.game.HUD.getItemValue("element")!=3){
			me.game.HUD.setItemValue("element", 3);
			// reset everything to stone values
			me.game.getEntityByName("mainPlayer")[0].fullReset();
		}
	}
 
});

/*----------------
Metal totem 4
------------------------ */
game.MetalTotem = me.CollectableEntity.extend({
    // extending the init function is not mandatory
    // unless you need to add some extra initialization
    init: function(x, y, settings) {
        // call the parent constructor
        this.parent(x, y, settings);
    },
 
    // this function is called by the engine, when
    // an object is touched by something (here collected)
	onCollision : function ()
	{
		if(me.game.HUD.getItemValue("element")!=4){
			me.game.HUD.setItemValue("element", 4);
			// reset everything to stone values
			me.game.getEntityByName("mainPlayer")[0].fullReset();
		}
	}
 
});

/*----------------
Electricity totem 5
------------------------ */
game.ElecTotem = me.CollectableEntity.extend({
    // extending the init function is not mandatory
    // unless you need to add some extra initialization
    init: function(x, y, settings) {
        // call the parent constructor
        this.parent(x, y, settings);
    },
 
    // this function is called by the engine, when
    // an object is touched by something (here collected)
	onCollision : function ()
	{
		if(me.game.HUD.getItemValue("element")!=5){
			me.game.HUD.setItemValue("element", 5);
			// reset everything to stone values
			me.game.getEntityByName("mainPlayer")[0].fullReset();
		}
	}
 
});

/*----------------
 Several obstacles in the game
------------------------ */

/*----------------
Firefloor
------------------------ */
game.FireEntity = me.CollectableEntity.extend({
    // extending the init function is not mandatory
    // unless you need to add some extra initialization
    init: function(x, y, settings) {
        // call the parent constructor
        this.parent(x, y, settings);
    },
 
    // this function is called by the engine, when
    // an object is touched by something (here collected)
	onCollision : function ()
	{
		if((me.game.HUD.getItemValue("element")==2)&&(game.persistent.element_active==true)){
			me.game.remove(this);
		}
		else{
			me.levelDirector.reloadLevel();
		}		
	}
 
});

/*----------------
Tree
------------------------ */
game.TreeEntity = me.CollectableEntity.extend({
    // extending the init function is not mandatory
    // unless you need to add some extra initialization
	
    init: function(x, y, settings) {
        // call the parent constructor
        this.parent(x, y, settings);
		this.renderable.addAnimation("safe", [0]);
		this.renderable.addAnimation("burning", [1,2,3,4,5,1,2,3,4,5]);
		this.renderable.addAnimation("burned", [6]);
		this.renderable.animationspeed=me.sys.fps/1;
		this.renderable.setCurrentAnimation("safe");
		this.burned=false;
    },
 
    // this function is called by the engine, when
    // an object is touched by something (here collected)
	onCollision : function ()
	{
		/* me.game.getEntityByName("mainPlayer")[0].vel.x = -me.game.getEntityByName("mainPlayer")[0].vel.x; */
		if((me.game.HUD.getItemValue("element")==1)&&(game.persistent.element_active==true)&&(!this.burned)){
			this.renderable.setCurrentAnimation("burning","burned");
			this.burned=true;
		}	
	}
 
});

/*----------------
Lake
------------------------ */
game.LakeEntity = me.CollectableEntity.extend({
    // extending the init function is not mandatory
    // unless you need to add some extra initialization
    init: function(x, y, settings) {
        // call the parent constructor
        this.parent(x, y, settings);
    },
 
    // this function is called by the engine, when
    // an object is touched by something (here collected)
	onCollision : function ()
	{
		if(!((me.game.HUD.getItemValue("element")==2)&&(game.persistent.element_active==true))){
			me.levelDirector.reloadLevel();		
		}		
	}
 
});

/*--
TeleportIN
--*/
game.TeleportINEntity = me.CollectableEntity.extend({
    init: function(x, y, settings){
        this.parent(x, y, settings);
    },
    
    onCollision : function () 
    {
        if((me.game.HUD.getItemValue("element")==game.ElemEnum.ELECTRIC)&&(game.persistent.element_active==true)){
            //console.log(me.game.getEntityByName("TeleportOUT")[0]);
            me.game.getEntityByName("mainPlayer")[0].pos.x = me.game.getEntityByName("TeleportOUT")[0].pos.x;
            me.game.getEntityByName("mainPlayer")[0].pos.y = me.game.getEntityByName("TeleportOUT")[0].pos.y;
		}
    }
    
});

/*--
TeleportOUT
--*/
game.TeleportOUTEntity = me.CollectableEntity.extend({
    init: function(x, y, settings){
        this.parent(x, y, settings);
    },
    
    onCollision : function () 
    {
        //me.game.getEntityByName("mainPlayer")[0].x = me.game.getEntityByName("TeleportINEntity")[0].x;
        //me.game.getEntityByName("mainPlayer")[0].y = me.game.getEntityByName("TeleportINEntity")[0].y;
    }
    
});

/*--
Go to endscreen
--*/
game.EndEntity = me.CollectableEntity.extend({
    init: function(x, y, settings){
        this.parent(x, y, settings);
    },
    
    onCollision : function () 
    {
		me.state.change(me.state.GAMEWON);
    }
    
});