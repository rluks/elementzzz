game.resources = [
	/**
	 * Graphics.
	 */
	// the main player spritesheet
	{name: "player_sprite",     type:"image",	src: "data/img/sprite/player_sprite.png"},
	// the spinning coin spritesheet
	{name: "spinning_coin_gold",  type:"image",	src: "data/img/sprite/spinning_coin_gold.png"},
	// our enemty entity
	{name: "wheelie_right",       type:"image",	src: "data/img/sprite/wheelie_right.png"},
	// game font
	{name: "32x32_font",          type:"image",	src: "data/img/font/32x32_font.png"},
	// title screen
	{name: "title_screen",        type:"image",	src: "data/img/gui/title_screen.png"},
	{name: "ending_screen",        type:"image",	src: "data/img/gui/ending_screen.png"},
	// the parallax background
	{name: "area01_bkg0",         type:"image",	src: "data/img/area01_bkg0.png"},
	{name: "area01_bkg1",         type:"image",	src: "data/img/area01_bkg1.png"},
	{name: "background1",         type:"image",	src: "data/img/background1.png"},
	{name: "background2",         type:"image",	src: "data/img/background2.png"},
	{name: "background3",         type:"image",	src: "data/img/background3.png"},
	{name: "background4",         type:"image",	src: "data/img/background4.png"},
	{name: "background5",         type:"image",	src: "data/img/background5.png"},
    {name: "background6",         type:"image",	src: "data/img/background6.png"},
	{name: "background7",         type:"image",	src: "data/img/background7.png"},
	
	// our level tileset
	{name: "area01_level_tiles",  type:"image",	src: "data/img/map/area01_level_tiles.png"},
	{name: "tiles1",  type:"image",	src: "data/img/map/tiles1.png"},
	{name: "water_tiles",  type:"image",	src: "data/img/map/water_tiles.png"},
	
	//Elements
	{name: "Candle",  type:"image",	src: "data/img/sprite/Candle.png"},
	{name: "Glass",  type:"image",	src: "data/img/sprite/Glass.png"},
	{name: "Fan",  type:"image",	src: "data/img/sprite/Fan.png"},
	{name: "Hummer",  type:"image",	src: "data/img/sprite/Hummer.png"},
	{name: "Battery",  type:"image",	src: "data/img/sprite/Battery.png"},
	
	//exit to next room
	{name: "Exit",  type:"image",	src: "data/img/sprite/Exit.jpg"},
    
    //teleport
	{name: "teleport",  type:"image",	src: "data/img/map/teleport.png"},
    
	// HUD images
	{name: "Eslot_empty",  type:"image",	src: "data/img/HUD/Eslot_empty.png"},
	{name: "Eslot_fire",  type:"image",		src: "data/img/HUD/Eslot_fire.png"},
	{name: "Eslot_metal",  type:"image",	src: "data/img/HUD/Eslot_metal.png"},
	{name: "Eslot_wind",  type:"image",		src: "data/img/HUD/Eslot_wind.png"},
	{name: "Eslot_elec",  type:"image",		src: "data/img/HUD/Eslot_elec.png"},
	{name: "Eslot_water",  type:"image",	src: "data/img/HUD/Eslot_water.png"},
	
	// obstacles
	{name: "fire",  type:"image",	src: "data/img/sprite/fire.png"},
	{name: "tree",  type:"image",	src: "data/img/sprite/tree.png"},
	
	//teleport
	{name: "teleport",  type:"image",	src: "data/img/map/teleport.png"},
	
	/* 
	 * Maps. 
 	 */
	{name: "area01",              type: "tmx",	src: "data/map/area01.tmx"},
	{name: "area02",              type: "tmx",	src: "data/map/area02.tmx"},
	{name: "area03",              type: "tmx",	src: "data/map/area03.tmx"},
	{name: "area04",              type: "tmx",	src: "data/map/area04.tmx"},
	{name: "area05",              type: "tmx",	src: "data/map/area05.tmx"},
    {name: "area06",              type: "tmx",	src: "data/map/area06.tmx"},
	{name: "area07",              type: "tmx",	src: "data/map/area07.tmx"},
	{name: "area08",              type: "tmx",	src: "data/map/area08.tmx"},

	/* 
	 * Background music. 
	 */	
	{name: "dst-inertexponent", type: "audio", src: "data/bgm/", channel : 1},
    {name: "janek_wisniewski_padl", type: "audio", src: "data/bgm/", channel : 2},
    {name: "SkyroadsRoad02", type: "audio", src: "data/bgm/", channel : 2},
    {name: "SkyroadsRoad08", type: "audio", src: "data/bgm/", channel : 2},
    {name: "Wayfinder_on_the_road_8", type: "audio", src: "data/bgm/", channel : 2},
	
	/* 
	 * Sound effects. 
	 */
	{name: "cling", type: "audio", src: "data/sfx/", channel : 2},
	{name: "stomp", type: "audio", src: "data/sfx/", channel : 1},
    {name: "burp2", type: "audio", src: "data/sfx/", channel : 1},
	{name: "burp1", type: "audio", src: "data/sfx/", channel : 1},
    {name: "gameover", type: "audio", src: "data/sfx/", channel : 2},
	{name: "jump",  type: "audio", src: "data/sfx/", channel : 1}
];
