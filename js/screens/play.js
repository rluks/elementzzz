/* the in game stuff*/
game.PlayScreen = me.ScreenObject.extend({
 
    onResetEvent: function() {
        // load a level
        me.levelDirector.loadLevel("area01");
 
        // add a default HUD to the game mngr
        me.game.addHUD(0, 0, 640, 480);
        // add a new HUD item
		me.game.HUD.addItem("element", new game.ElementSlot(572, 20));
        
        var randomnumber=Math.floor(Math.random()*5);
        me.audio.stopTrack();
        switch(randomnumber){
            case 0: me.audio.playTrack("DST-InertExponent");
                    break;
            case 1: me.audio.playTrack("janek_wisniewski_padl");
                    break;
            case 2: me.audio.playTrack("SkyroadsRoad02");
                    break;
            case 3: me.audio.playTrack("SkyroadsRoad08");
                    break;   
            default: me.audio.playTrack("Wayfinder_on_the_road_8");
                    break;                    
        }

 
        // make sure everything is in the right order
        me.game.sort();
 
    },
 
    /* ---
 
    action to perform when game is finished (state change)
 
    --- */
    onDestroyEvent: function() {
        // remove the HUD
        me.game.disableHUD();
        me.audio.stopTrack();
    }
 
});
